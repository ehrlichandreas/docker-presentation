[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/ehrlichandreas/docker-presentation?grs=bitbucket#)

```
https://gitpitch.com/ehrlichandreas/docker-presentation?grs=bitbucket#
```

Start local web server

```shell
#!/bin/sh

python -m SimpleHTTPServer
```

or

```cmd
php -S localhost:8000 index.html
```
