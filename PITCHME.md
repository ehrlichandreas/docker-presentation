# Toolbox Docker
### For Weblogic, MCE and nearly everything else

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/ehrlichandreas/docker-presentation?grs=bitbucket#)

Note:
Remember to speak english if possible.

---

### Agenda

- What is Docker
- Docker vs Virtual Machines vs LXC (Linux Container)
- Advantages of Docker containers
- Limitations to using Docker
- What is a Dockerfile
- Why use docker compose
- Real life example

---

### What is Docker

#### Quick explanation

![Image](./assets/md/assets/down-arrow.png)

+++

- The IT software "Docker” is containerization technology that enables the creation and use of Linux® containers.
- The open source Docker community works to improve these technologies to benefit all users—freely.
- The company, Docker Inc., builds on the work of the Docker community, makes it more secure, and shares those advancements back to the greater community. It then supports the improved and hardened technologies for enterprise customers.

+++

### What is Docker

#### What Docker is good for

![Image](./assets/md/assets/down-arrow.png)

+++

##### What Docker is good for

With DOCKER, you can treat containers like extremely lightweight, modular virtual machines. And you get flexibility with those containers—you can create, deploy, copy, and move them from environment to environment.

+++

### What is Docker

#### How does Docker work

![Image](./assets/md/assets/down-arrow.png)

+++

##### How does Docker work

The Docker technology uses the Linux kernel and features of the kernel, like Cgroups and namespaces, to segregate processes so they can run independently.
Sounds like LXC (traditional Linux Containers), but Docker uses own library for container and image operations to provide a better user experience.
Docker creates a layer for each step defined in Dockerfile, which summarized give you an image.
When you start a container of an image, you get a running container.

---

### Docker vs Virtual Machines vs LXC (Linux Containers)

#### Docker vs Virtual Machines

![Image](./assets/md/assets/down-arrow.png)

+++

#### Docker vs Virtual Machines

![Image](./assets/md/assets/docker-vs-virtual-machines.png)

+++

##### Docker vs Virtual Machines

- No Hypervisor
- No Guest OS
- Appilaction works directly on Host Kernel

+++

### Docker vs Virtual Machines vs LXC (Linux Container)

#### Docker vs LXC (Linux Containers)

![Image](./assets/md/assets/down-arrow.png)

+++

#### Docker vs LXC (Linux Containers)

![Image](./assets/md/assets/traditional-linux-containers-vs-docker_2.png)

---

### Advantages of Docker containers

#### Modularity

![Image](./assets/md/assets/down-arrow.png)

+++

##### Modularity

The Docker approach to containerization is focused on the ability to take down a part of an application, to update or repair, without unnecessarily taking down the whole app. In addition to this microservices-based approach, you can share processes amongst multiple apps in much the same way that service-oriented architecture (SOA) works.

+++

### Advantages of Docker containers

#### Layers and image version control

![Image](./assets/md/assets/down-arrow.png)

+++

##### Layers and image version control

Each Docker image file is made up of a series of layers. These layers are combined into a single image. A layer is created when the image changes. Every time a user specifies a command, such as run or copy, a new layer gets created.

+++

##### Layers and image version control (2)

Docker reuses these layers for new container builds, which makes the build process much faster.Intermediate changes are shared between images, further improving speed, size, and efficiency. Inherent to layering is version control. Every time there’s a new change, you essentially have a built-in changelog—full control over your container images.

+++

##### Layers and image version control (sum.)

Version control and component reuse – you can track successive versions of a container, inspect differences, or roll-back to previous versions. Containers reuse components from the preceding layers, which makes them noticeably lightweight.

+++

### Advantages of Docker containers

#### Rollback

![Image](./assets/md/assets/down-arrow.png)

+++

##### Rollback

Every image has layers. Don’t like the current iteration of an image? Roll it back to the previous version. This supports an agile development approach and helps make continuous integration and deployment (CI/CD) a reality from a tools perspective.

+++

### Advantages of Docker containers

#### Rapid deployment

![Image](./assets/md/assets/down-arrow.png)

+++

##### Rapid deployment (1)

Getting new hardware up, running, provisioned, and available used to take days. And the level of effort and overhead was burdensome. Docker-based containers can reduce deployment to seconds. By creating a container for each process, you can quickly share those similar processes with new apps. And, since an OS doesn’t need to boot to add or move a container, deployment times are substantially shorter. On top of this, with the speed of deployment, you can easily and cost-effectively create and destroy data created by your containers without concern.

+++

##### Rapid deployment (sum.)

Rapid application deployment – containers include the minimal runtime requirements of the application, reducing their size and allowing them to be deployed quickly.

+++

### Advantages of Docker containers

#### Portability across machines

![Image](./assets/md/assets/down-arrow.png)

+++

##### Portability across machines

An application and all its dependencies can be bundled into a single container that is independent from the host version of Linux kernel, platform distribution, or deployment model. This container can be transfered to another machine that runs Docker, and executed there without compatibility issues.

+++

### Advantages of Docker containers

#### Sharing

![Image](./assets/md/assets/down-arrow.png)

+++

##### Sharing

You can use a remote repository to share your container with others. Docker provides a registry for this purpose, and it is also possible to configure your own private repository for example with Gitlab.

+++

### Advantages of Docker containers

#### Lightweight footprint and minimal overhead

![Image](./assets/md/assets/down-arrow.png)

+++

##### Lightweight footprint and minimal overhead

Docker images are typically very small, which facilitates rapid delivery and reduces the time to deploy new application containers.

+++

### Advantages of Docker containers

#### Simplified maintenance

Docker reduces effort and risk of problems with application dependencies.

---

### Limitations to using Docker

- Docker is good but cannot completely replace virtual machines.
- Some Linux-Images can't work directly on MS Server.
- There are some applications that cannot be perfectly deployed in containers. Docker is good for micro-services.
- Tools to manage and monitor containers are limited as of now.
- Sometimes difficult orchestration.

---

### What is a Dockerfile

The Dockerfile is essentially the build instructions to build the Docker image.

The advantage of a Dockerfile over just storing the binary image (or a snapshot/template in other virtualization systems) is that the automatic builds will ensure you get what you defined. This is a good thing from a security perspective, as you want to ensure you’re not installing any vulnerable software.

![Image](./assets/md/assets/down-arrow.png)

+++

### What is a Dockerfile
#### Example (Dockerfile)

```shell
FROM debian:stretch-slim
LABEL maintainer="Andreas Ehrlich <ehrlich.andreas@googlemail.com>"
RUN export DEBIAN_FRONTEND=noninteractive && \
  apt-get update --allow-unauthenticated && \
  apt-get install -y --fix-missing \
    --no-install-recommends \
    --allow-unauthenticated apt-utils && \
  apt-get update --allow-unauthenticated && \
  apt-get install -y --fix-missing \
    --no-install-recommends --allow-unauthenticated \
    grep wget tar git ssh ca-certificates lsof sudo cpio bash
VOLUME /var/www/html
...
```

<span class="code-presenting-annotation fragment current-only" data-code-focus="1">Tells us what image to base this off of</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="2">Label of the image, is like a tag</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="3-11">RUN command which runs a "shell" command during the build process</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="12">Allows to mount external path in this directory</span>

---

### Why use docker compose

- Docker cli is good for managing individual containers on a docker engine by accessing the docker daemon api.
- The docker-compose cli can be used to manage a multi-container application.
  - Many options moved into the docker-compose.yml file for easier reuse.
  - Works as a front end "script" on top of the same docker api used by docker.

+++

### Why use docker compose
#### Example (docker-compose.yaml)

```yaml
version: '3.3'
services:
    webapp:
        image: "php:5.6.32-apache"
        restart: always
        ports:
            - 80:80
        links:
            - mysqldb:database
        environment:
            MYSQL_USER: testuser
            MYSQL_PASSWORD: testpass
            MYSQL_DATABASE: testbase
            MYSQL_SERVER: database
        network_mode: bridge
        volumes:
            - ./website:/var/www/html
    mysqldb:
        image: "mysql:5.5.57"
        restart: always
        expose:
            - 3306
        environment:
            MYSQL_ROOT_PASSWORD: root
            MYSQL_USER: testuser
            MYSQL_PASSWORD: testpass
            MYSQL_DATABASE: testbase
        network_mode: bridge
        volumes:
            - ./mysql-data:/var/lib/mysql
...
```

<span class="code-presenting-annotation fragment current-only" data-code-focus="1">Compose format version</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="2">Which services (php web application)</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="3-17">Description of the commands to be mapped to docker cli</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="18">Which services (mysql database system)</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="19-30">Description of the commands to be mapped to docker cli</span>

---

## Real life example

- o2-mce/debian:stretch-slim
- o2-mce/oraclejdk:1.8.0-112
- o2-mce/maven:3.2.3
- build.sh
- maven dockerized wrapper
- useage of maven dockerized wrapper

+++

### Real life example
#### o2-mce/debian:stretch-slim

```shell
FROM debian:stretch-slim
LABEL maintainer="Andreas Ehrlich <ehrlich.andreas@googlemail.com>"


ENV DEBIAN_FRONTEND noninteractive
ARG DEBIAN_FRONTEND=noninteractive


RUN \
export DEBIAN_FRONTEND=noninteractive && \
\
\
echo '# Setting additional sources' && \
    if ! grep -q "^deb .*mucsgdp07.sg.de.pri.o2.com/ubuntu" /etc/apt/sources.list /etc/apt/sources.list.d/*; then \
        echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
        LINE="\n## Cognizant dev tools" && \
        FILE="/etc/apt/sources.list" && \
        grep -qF "$LINE" "$FILE" || echo "$LINE" | tee --append "$FILE" && \
        LINE="deb http://mucsgdp07.sg.de.pri.o2.com/ubuntu cognizant-dev-tools non-free" && \
        FILE="/etc/apt/sources.list" && \
        grep -qF "$LINE" "$FILE" || echo "$LINE" | tee --append "$FILE"; \
    fi && \
echo '# Set additional sources' && \
\
\
echo '# Updating apt' && \
    DEBIAN_FRONTEND=noninteractive && \
    apt-get update --allow-unauthenticated && \
echo '# Updated apt' && \
\
\
echo '# Installing OS dependencies' && \
    apt-get install -y --fix-missing \
        --no-install-recommends --allow-unauthenticated \
        apt-utils && \
    apt-get update --allow-unauthenticated && \
    \
    apt-get install -y --fix-missing \
        --no-install-recommends --allow-unauthenticated \
        grep wget tar git ssh ca-certificates lsof sudo cpio bash && \
echo '# Installed OS dependencies' && \
\
\
echo '# Cleaning up' && \
    apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
echo '# Cleaned up' && \
\
\
echo '# Creating user: developer' && \
    mkdir -p /home/developer && \
    echo "developer:x:1000:1000:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:1000:" >> /etc/group && \
    sudo echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    sudo chmod 0440 /etc/sudoers.d/developer && \
    sudo chown -Rf developer:developer /home/developer && \
    sudo chown root:root /usr/bin/sudo && \
    chmod 4755 /usr/bin/sudo && \
echo '# Created user: developer' && \
\
echo '# Edit sudoers file' && \
echo '# To avoid error: sudo: sorry, you must have a tty to run sudo' && \
    sed -i -e "s/Defaults    requiretty.*/ #Defaults    requiretty/g" /etc/sudoers && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
echo '# Edited sudoers file'
```

<span class="code-presenting-annotation fragment current-only" data-code-focus="1-2">Basic image an label</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="13-23">Setting additional sources</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="26-29">Updating apt</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="32-41">Installing OS dependencies</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="44-49">Cleaning up</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="52-61">Creating user: developer</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="63-67">Edit sudoers file</span>

+++

### Real life example
#### o2-mce/oraclejdk:1.8.0-112

```shell
FROM o2-mce/debian:stretch-slim
LABEL maintainer="Andreas Ehrlich <ehrlich.andreas@googlemail.com>"


RUN \
export DEBIAN_FRONTEND=noninteractive && \
\
\
echo '# Installing oracle jdk' && \
    DEBIAN_FRONTEND=noninteractive && \
    apt-get update --allow-unauthenticated && \
    apt-get install -y --no-install-recommends --allow-unauthenticated \
        cog-jdk%%ORACLEJDKVERSION%%.x86-64 && \
echo '# Installed oracle jdk' && \
\
\
echo '# Cleaning up' && \
    apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
echo '# Cleaned up' && \
\
\
mkdir -pv "/opt/apps/oraInventory" && \
\
\
export JAVA_HOME="/opt/apps/java/jdk%%JDKVERSION%%" && \
export PATH="$PATH:$JAVA_HOME/bin" && \
\
\
echo '# Creating symlinks' && \
    rm -f "/usr/bin/java" && \
    rm -f "/usr/local/bin/java" && \
    rm -f "/usr/bin/javac" && \
    rm -f "/usr/local/bin/javac" && \
    rm -f "/usr/bin/javaw" && \
    rm -f "/usr/local/bin/javaw" && \
    \
    ln -s "${JAVA_HOME}/bin/java" "/usr/bin/java" && \
    ln -s "/usr/bin/java" "/usr/local/bin/java" && \
    ln -s "${JAVA_HOME}/bin/javac" "/usr/bin/javac" && \
    ln -s "/usr/bin/javac" "/usr/local/bin/javac" && \
    ln -s "${JAVA_HOME}/bin/javaw" "/usr/bin/javaw" && \
    ln -s "/usr/bin/javaw" "/usr/local/bin/javaw" && \
echo '# Created symlinks'
```

<span class="code-presenting-annotation fragment current-only" data-code-focus="1-2">Basic image and label</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="9-14">Installing oracle jdk</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="17-22">Cleaning up</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="28-29">Setting environment variables</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="32-46">Creating symlinks</span>

+++

### Real life example
#### o2-mce/maven:3.2.3

```shell
FROM %%ORACLEJDKTAG%%
LABEL maintainer="Andreas Ehrlich <ehrlich.andreas@googlemail.com>"


RUN \
export DEBIAN_FRONTEND=noninteractive && \
\
\
echo '# Installing maven' && \
    DEBIAN_FRONTEND=noninteractive && \
    apt-get update --allow-unauthenticated && \
    apt-get install -y --no-install-recommends --allow-unauthenticated \
        cog-maven-%%MAVENVERSION%% && \
echo '# Installed maven' && \
\
\
echo '# Cleaning up' && \
    apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
echo '# Cleaned up' && \
\
\
export MAVEN_PATH="/opt/apps/apache-maven-%%MAVENVERSION%%" && \
export M2_HOME="$MAVEN_PATH" && \
export PATH="$PATH:$MAVEN_PATH/bin" && \
\
\
echo '# Creating symlinks' && \
    rm -f "/usr/bin/mvn" && \
    rm -f "/usr/local/bin/mvn" && \
    \
    ln -s "$M2_HOME/bin/mvn" "/usr/bin/mvn" && \
    ln -s "/usr/bin/mvn" "/usr/local/bin/mvn" && \
echo '# Created symlinks'
```

<span class="code-presenting-annotation fragment current-only" data-code-focus="1-2">Basic image and label</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="9-14">Installing maven</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="17-22">Cleaning up</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="25-27">Setting environment variables</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="30-36">Creating symlinks</span>

+++

### Real life example
#### build.sh

```shell
#!/bin/sh

PREFIX="o2-mce"
ORACLEJDKVERSION="1.8.0-112"
MAVENVERSION="3.2.3"
WEBLOGICVERSION="12.2.1"
INSTANTCLIENTVERSION="11.2.0.1"
DEVRELEASE="R18-1"
USER="developer"
USERHOME="/home/developer"
PROJECTHOME="/home/developer"

GITUSER="%%USER%%"
GITPASS="%%PASS%%"

GITMCEREPO="https://%%GITUSER%%:%%GITPASS%%@%%MCEREPO%%"
GITDPCASEREPO="https://%%GITUSER%%:%%GITPASS%%@%%DPCASEREPO%%"

SOURCE="$0"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
DIR="$( realpath ${DIR} )"


escape() {
    echo "$1" | sed -e 's/[]\/$*.^|[]/\\&/g'
}


ORACLEJDKTAG="${PREFIX}/oraclejdk:${ORACLEJDKVERSION}"
ORACLEJDKLATEST="${PREFIX}/oraclejdk:latest"
ORACLEJDKTAGESCAPED=$(escape "${ORACLEJDKTAG}")

MAVENTAG="${PREFIX}/maven:${MAVENVERSION}"
MAVENLATEST="${PREFIX}/maven:latest"
MAVENTAGESCAPED=$(escape "${MAVENTAG}")

WEBLOGICTAG="${PREFIX}/weblogic:${WEBLOGICVERSION}"
WEBLOGICLATEST="${PREFIX}/weblogic:latest"
WEBLOGICTAGESCAPED=$(escape "${WEBLOGICTAG}")

WEBLOGICDEVTAG="${PREFIX}/weblogic-dev:${DEVRELEASE}"
WEBLOGICDEVLATEST="${PREFIX}/weblogic-dev:latest"
WEBLOGICDEVTAGESCAPED=$(escape "${WEBLOGICDEVTAG}")

WEBLOGICDEPLOYTAG="${PREFIX}/weblogic-deploy:${DEVRELEASE}"
WEBLOGICDEPLOYTAGESCAPED=$(escape "${WEBLOGICDEPLOYTAG}")

DEVRELEASEESCAPED=$(escape "${DEVRELEASE}")

GITUSERESCAPED=$(escape "${GITUSER}")
GITPASSESCAPED=$(escape "${GITPASS}")

GITMCEREPOESCAPED=$(escape "${GITMCEREPO}")
GITDPCASEREPOESCAPED=$(escape "${GITDPCASEREPO}")

JDKVERSION=$(echo "${ORACLEJDKVERSION}" | sed 's/-/_/g')
JDKVERSIONESCAPED=$(escape "${JDKVERSION}")

USERHOMEESCAPED=$(escape "${USERHOME}")
PROJECTHOMEESCAPED=$(escape "${PROJECTHOME}")


echo 'create basic debian image' && \
    cat "${DIR}/01-debian/Dockerfile.template" \
        > "${DIR}/01-debian/Dockerfile" && \
    \
    docker build \
        "${DIR}/01-debian" \
        -t "o2-mce/debian:stretch-slim" \
        --network "host" \
        --force-rm \
        --rm && \
    \
    rm "${DIR}/01-debian/Dockerfile" && \
echo 'created basic debian image' && \
\
\
echo 'create basic oracle jdk image' && \
    cat "${DIR}/02-oraclejdk/Dockerfile.template" | \
        sed "s/%%JDKVERSION%%/${JDKVERSIONESCAPED}/g" | \
        sed "s/%%ORACLEJDKVERSION%%/${ORACLEJDKVERSION}/g" \
        > "${DIR}/02-oraclejdk/Dockerfile" && \
    \
    docker build \
        "${DIR}/02-oraclejdk" \
        -t "${ORACLEJDKTAG}" \
        -t "${ORACLEJDKLATEST}" \
        --network "host" \
        --force-rm \
        --rm && \
    \
    rm "${DIR}/02-oraclejdk/Dockerfile" && \
echo 'created basic oracle jdk image' && \
\
\
echo 'create basic maven image' && \
    cat "${DIR}/03-maven/Dockerfile.template" | \
        sed "s/%%MAVENVERSION%%/${MAVENVERSION}/g" | \
        sed "s/%%ORACLEJDKTAG%%/${ORACLEJDKTAGESCAPED}/g" \
        > "${DIR}/03-maven/Dockerfile" && \
    \
    docker build \
        "${DIR}/03-maven" \
        -t "${MAVENTAG}" \
        -t "${MAVENLATEST}" \
        --network "host" \
        --force-rm \
        --rm && \
    \
    rm "${DIR}/03-maven/Dockerfile" && \
echo 'created basic maven image' && \
\
\
echo 'create weblogic image' && \
    cat "${DIR}/04-weblogic/Dockerfile.template" | \
        sed "s/%%MAVENTAG%%/${MAVENTAGESCAPED}/g" | \
        sed "s/%%INSTANTCLIENTVERSION%%/${INSTANTCLIENTVERSION}/g" | \
        sed "s/%%WEBLOGICVERSION%%/${WEBLOGICVERSION}/g" \
        > "${DIR}/04-weblogic/Dockerfile" && \
    \
    docker build \
        "${DIR}/04-weblogic" \
        -t "${WEBLOGICTAG}" \
        -t "${WEBLOGICLATEST}" \
        --network "host" \
        --force-rm \
        --rm && \
    \
    rm "${DIR}/04-weblogic/Dockerfile" && \
echo 'created weblogic image' && \
\
\
echo 'create mce image' && \
    cat "${DIR}/05-mce-project/startserver.template" | \
        sed "s/%%DEVRELEASE%%/${DEVRELEASEESCAPED}/g" \
        > "${DIR}/05-mce-project/startserver.sh" && \
\
    cat "${DIR}/05-mce-project/Dockerfile.template" | \
        sed "s/%%WEBLOGICTAG%%/${WEBLOGICTAGESCAPED}/g" | \
        sed "s/%%DEVRELEASE%%/${DEVRELEASEESCAPED}/g" | \
        sed "s/%%GITMCEREPO%%/${GITMCEREPOESCAPED}/g" | \
        sed "s/%%GITDPCASEREPO%%/${GITDPCASEREPOESCAPED}/g" | \
        sed "s/%%GITUSER%%/${GITUSERESCAPED}/g" | \
        sed "s/%%GITPASS%%/${GITPASSESCAPED}/g" | \
        sed "s/%%JDKVERSION%%/${JDKVERSIONESCAPED}/g" | \
        sed "s/%%MAVENVERSION%%/${MAVENVERSION}/g" | \
        sed "s/%%USERHOME%%/${USERHOMEESCAPED}/g" | \
        sed "s/%%USER%%/${USER}/g" | \
        sed "s/%%PROJECTHOME%%/${PROJECTHOMEESCAPED}/g" \
        > "${DIR}/05-mce-project/Dockerfile" && \
\
    docker build \
        "${DIR}/05-mce-project" \
        -t "${WEBLOGICDEVTAG}" \
        -t "${WEBLOGICDEVLATEST}" \
        --network "host" \
        --force-rm \
        --rm && \
\
    rm "${DIR}/05-mce-project/Dockerfile" && \
    rm "${DIR}/05-mce-project/startserver.sh" && \
echo 'created mce image'
#
#
#cat "${DIR}/weblogic-deploy/Dockerfile.template" | \
#    sed "s/%%DEVRELEASE%%/${DEVRELEASEESCAPED}/g" | \
#    sed "s/%%WEBLOGICDEVTAG%%/${WEBLOGICDEVTAGESCAPED}/g" \
#    > "${DIR}/weblogic-deploy/Dockerfile"
#
#docker build \
#    "${DIR}/weblogic-deploy" \
#    -t "${WEBLOGICDEPLOYTAG}" \
#    --network "host" \
#    --force-rm \
#    --rm
#
#rm "${DIR}/weblogic-deploy/Dockerfile"

```

<span class="code-presenting-annotation fragment current-only" data-code-focus="3-11">Predefine variables needed for build process</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="13-17">Predefine variables needed for build process</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="19-26">Get directory of the current file</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="34-44">Compute needed variables from predefined</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="46-53">Compute needed variables from predefined</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="55-65">Compute needed variables from predefined</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="68-80">Create basic debian image</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="83-95">Create basic oracle jdk image</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="101-113">Create basic maven image</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="119-132">Create basic weblogic image</span>

+++

### Real life example
#### maven dockerized wrapper (mvnwd.sh)

```shell
#!/bin/sh

DOCKERIMAGETAG="maven:3.5.0-jdk-8-alpine"
#DOCKERIMAGETAG="o2-mce/maven:3.2.3"


WORKDIR="$( readlink -f $( pwd ) )"
WORKDIR="$( realpath ${WORKDIR} )"

MOUNTDIR="$(git rev-parse --show-toplevel)"
MOUNTDIR="$( realpath ${MOUNTDIR} )"

USERDIR="/home/maven"

MAVEN_OPTS=" \
    -Duser.home=${USERDIR} \
    -Xms512m \
    -Xmx2000m \
    -XX:MetaspaceSize=256m \
    -XX:MaxMetaspaceSize=2048m \
    -XX:+UseParallelGC "


docker run --rm \
    --net=host \
    --user "$(id -u):$(id -g)"  \
    --env "HOME=${USERDIR}" \
    --env "MAVEN_CONFIG=${USERDIR}/.m2" \
    --env "MAVEN_OPTS=${MAVEN_OPTS}" \
    -v "${HOME}/.m2:${USERDIR}/.m2" \
    -v "${HOME}/.ssh:${USERDIR}/.ssh" \
    -v "${MOUNTDIR}:${MOUNTDIR}" \
    -w "${WORKDIR}" \
    "${DOCKERIMAGETAG}" \
    mvn $@
```

<span class="code-presenting-annotation fragment current-only" data-code-focus="3-4">Choose a docker image</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="7-13">Set working directories</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="15-21">Set maven options</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="24-35">Execute maven inside docker</span>

+++

### Real life example
#### useage of maven dockerized wrapper (mvnwd-build.sh)

```shell
#!/bin/sh

SOURCE="$0"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
DIR="$( realpath ${DIR} )"

WORKDIR="$( readlink -f $( pwd ) )"
WORKDIR="$( realpath ${WORKDIR} )"

PROJECTDIR="${DIR}/../.."
mkdir -pv ${PROJECTDIR}
PROJECTDIR="$( realpath ${PROJECTDIR} )"

#MAVENBIN="$( which mvn )"
MAVENBIN="${DIR}/mvnwd.sh"
MAVENBIN="$( realpath ${MAVENBIN} )"


cd ${PROJECTDIR}


sh ${MAVENBIN} -f ${PROJECTDIR}/pom.xml clean install $@


cd ${WORKDIR}
```

<span class="code-presenting-annotation fragment current-only" data-code-focus="3-10">Set working directories</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="12-21">Set working directories</span>
<span class="code-presenting-annotation fragment current-only" data-code-focus="27">Execute maven build process inside docker</span>
